class User<ActiveRecord::Base
  has_many :photos
  has_many :tags
  attr_accessor :password
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_confirmation_of :password
  before_save :encrypt_password
  validates :email, uniqueness: true

  validates :email, :presence => true
  def encrypt_password
    self.password_salt = BCrypt::Engine.generate_salt
    self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
  end

  def is_email?
    self.email.include?('@') && self.email.include?('.')
  end

  def self.authenticate(email, password)
    user = User.where(email: email).first
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def follows?(other_user)
    Followship.exists?(follower_id: self.id, followed_id: other_user.id)
  end
  
  def followees
    followee_ids = Followship.where(follower_id: self.id).map(&:followed_id)
    User.find(followee_ids)
  end

  def all_photos
    users = [self] + self.followees
    Photo.where(user_id: users.map(&:id)).order(created_at: :desc)
  end
end
