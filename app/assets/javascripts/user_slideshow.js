var UserSlideshow = {
  activeTimers: {},
  start: function() {
    const id = $(this).attr('id');
    console.log("Slideshow Start on elem with ID: " + id)
    var slides = $(this).find('.slides').children();
    var i = Math.min(slides.length - 1, 1);
    const currentSlide = $(this).find('.slide');
    const currentImage = $(currentSlide).find('img');
    const currentTitle = $(currentSlide).find('.title');
    UserSlideshow.activeTimers[id] = setInterval(function() {
      const wantedSlide = slides.get(slides.length - i - 1);
      $(currentImage).attr('src', $(wantedSlide).attr('src'));
      $(currentImage).attr('alt', $(wantedSlide).attr('alt'));
      $(currentImage).attr('data-modal-url', $(wantedSlide).attr('data-modal-url'));
      $(currentTitle).text($(wantedSlide).attr('alt'));
      i = (i + 1) % slides.length;
      console.log("Current AT: ")
      console.log(UserSlideshow.activeTimers)
    }, 1000);
  },
  stop: function() {
    const id = $(this).attr('id');
    console.log("Slideshow Stop on elem with ID: " + id)
    clearInterval(UserSlideshow.activeTimers[id]);
    delete UserSlideshow.activeTimers[id];
  },
  stopAll: function() {
    $('.slideshow').off('mouseenter mouseleave');
    for (let id of Object.keys(UserSlideshow.activeTimers)) {
      clearInterval(UserSlideshow.activeTimers[id]);
      delete UserSlideshow.activeTimers[id];
    }
  },
  setup: function() {
    $('.slideshow').hover(UserSlideshow.start, UserSlideshow.stop);
  }
};
$(UserSlideshow.setup);
