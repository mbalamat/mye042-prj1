var PhotoPopup = {
  setup: function() {
    $(document).on('click', '#photos .slide img', PhotoPopup.getPhotoInfo);
  }
  ,getPhotoInfo: function() {
    console.log('requesting from', $(this).attr('data-modal-url'));
    $.ajax({type: 'GET',
            url: $(this).attr('data-modal-url'),
            timeout: 5000,
            success: PhotoPopup.showPhotoInfo,
            error: function(xhrObj, textStatus, exception) { alert('Error!'); }
            // 'success' and 'error' functions will be passed 3 args
           });
    return false;
  }
  ,showPhotoInfo: function(data) {
    // add hidden 'div' to end of page to display popup:
    var popupDiv = $('<div id="photoInfo"></div>');
    popupDiv.hide().appendTo($('body'));
    UserSlideshow.stopAll();
    // center a floater 1/2 as wide and 1/4 as tall as screen
    var oneFourth = Math.ceil($(window).width() / 4);
    $('#photoInfo').
      css({'left': oneFourth,  'width': 2*oneFourth, 'top': 250}).
      html(data).
      show();
    $('#photoInfo img').dblclick(function() {
      if ($('#photoInfo input')[3].value === "Delete") {
        $('#photoInfo input')[3].click();
      }
    });
    // make the Close link in the hidden element work
    $('#closeLink').click(PhotoPopup.hidePhotoInfo);
    return false;  // prevent default link action
  }
  ,hidePhotoInfo: function() {
    $('#photoInfo').remove();
    return false;
  }
};
$(PhotoPopup.setup);
