class FollowshipsController < ApplicationController
  def create
    followship = Followship.create!(followship_params)
    redirect_to user_path(followship.followed_id)
  end

  private
  def followship_params
    params.require(:followship).permit(:follower_id,
                                       :followed_id)
  end
end
