class CommentsController < ApplicationController
  def create
    Comment.create!(text: comment_params[:text],
                    photo_id: comment_params[:photo_id],
                    user_id: session[:user_id])
    redirect_to :back
  end

  private
  def comment_params
    params.require(:comment).permit(:text, :photo_id)
  end
end
