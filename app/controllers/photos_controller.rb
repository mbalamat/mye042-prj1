class PhotosController < ApplicationController
  def create
    @user = User.find(params[:user_id])
    if params[:photo] == nil

      flash[:alert] = "Please upload a photo"
      redirect_to :back
    else
      @photo = Photo.create(photo_params)
      if @photo.valid?
        @photo.user_id = @user.id
        @photo.save
        flash[:notice] = "Successfully uploaded a photo"
        redirect_to user_path(@user)
      else
        flash[:alert] = "Missing fields"
        redirect_to :back
      end
    end
  end

  def new
    @user = User.find(params[:user_id])
    @photo = Photo.create()
  end

  def destroy
    photo = Photo.find(params[:id])
    if photo.user.id == session[:user_id]
      photo.destroy!
    end
    redirect_to :back
  end

  def show
    @photo = Photo.find(params[:id])
    @users = User.all
    @myself = session[:user_id]
    @tag = Tag.new
  end

  private
  def photo_params
    params.require(:photo).permit(:image, :title)
  end
end
